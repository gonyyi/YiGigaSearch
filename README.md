# YiGigaSearch

Example:
```
package main

import (
	"./YiGigaSearch"
	"fmt"
)

func main() {
	ggs := YiGigaSearch.YiGigaSearch{
		IndexLen:           10,
		InputFileName:      "./data/test.txt",
		InputFileDelimiter: ",",
		InputFileIndex:     0,
		PrintStatus:        false,
	}

	fmt.Println(ggs.CreateIndexFiles())

	fmt.Println(ggs.LoadIndexFiles())
	a, b := ggs.Search("1234567890130")
	fmt.Printf("(%s), (%s)", a, b)
}
```

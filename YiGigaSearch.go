/* --------------------------------------------------------------------------- *
  Title:		Gon Giga Search v1.0
  Author:		Gon Yi (gonyispam@gmail.com)
  Date:			Feb 4, 2016
  Description:	Indexing and Searching Large Text File
* --------------------------------------------------------------------------- */

package YiGigaSearch

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type YiGigaSearch struct {
	IndexLen           int
	InputFileName      string
	InputFileDelimiter string
	InputFileIndex     int
	PrintStatus        bool
	doubleIndex        map[string]int64
}

// *****************************************************************************
// SEARCHING INDEX FILE
// *****************************************************************************

func (g *YiGigaSearch) Search(key string) (string, error) {
	if _, ok := g.doubleIndex[key[0:g.IndexLen]]; ok { // g.doubleIndex[key[0:g.IndexLen]] : ranged index; where to start looking at from index
		// GETTING INDEX
		idxStr, err := g.SearchIndex(g.InputFileName+".ggs1", key, g.doubleIndex[key[0:g.IndexLen]]) // search within the ranged index
		if err != nil {
			return "", err
		}

		newInt64, err := strconv.ParseInt(strings.Split(strings.TrimSpace(idxStr), ",")[1], 10, 64)
		if err != nil {
			return "", err
		}
		// GETTING FULL RAW
		out, err := g.SearchIndex(g.InputFileName, key, newInt64)
		if err != nil {
			return "", err
		}
		return out, nil
	}
	return "", errors.New("No rec found")
}

func (g *YiGigaSearch) SearchIndex(file, key string, pos int64) (string, error) {
	fi, err := os.Open(file)
	if err != nil {
		return "", err
	}
	fi.Seek(pos, 0)
	bfi := bufio.NewReaderSize(fi, 1*1024*1024)

	defer fi.Close()
	lenKey := len(key)

	out, err := bfi.ReadString([]byte("\n")[0])
	if len(out) >= g.IndexLen && len(key) >= g.IndexLen {
		for out[0:g.IndexLen] == key[0:g.IndexLen] {
			if out[0:lenKey] == key {
				return strings.TrimSpace(out), nil
			}
			out, err = bfi.ReadString([]byte("\n")[0])
			if err != nil {
				return "", err
			}
			if len(out) < g.IndexLen {
				return "", errors.New("Key is too small")
			}
		}

	}
	// fmt.Println("Raw from searchIndex:", file, key, pos)
	return "", errors.New("No rec found")
}

// *****************************************************************************
// LOADING INDEX FILE
// *****************************************************************************
func (g *YiGigaSearch) LoadIndexFiles() (int, error) {
	g.doubleIndex = make(map[string]int64)
	count := 0
	fi, err := os.Open(g.InputFileName + ".ggs2")
	if err != nil {
		return count, errors.New(
			fmt.Sprintf("Cannot open dobule indexed file <%s>.", g.InputFileName+".ggs2"),
		)
	}
	bfi := bufio.NewScanner(fi)
	defer fi.Close()

	for bfi.Scan() {
		count += 1
		t := bfi.Text()
		ta := strings.Split(strings.TrimSpace(t), ",")

		newInt64, err := strconv.ParseInt(strings.TrimSpace(ta[1]), 10, 64)

		if err != nil {
			return count, errors.New(
				fmt.Sprintf("Having an error while loading double indexed file on line %d", count),
			)
		}
		g.doubleIndex[ta[0]] = newInt64
	}
	return count, nil
}

// *****************************************************************************
// CREATION OF INDEX FILE
// *****************************************************************************
func (g *YiGigaSearch) CreateIndexFiles() (int, error) {
	count := 0
	// -------------------------------------------------------------------------
	// OPEN AN INPUT FILE
	// -------------------------------------------------------------------------
	fi, err := os.Open(g.InputFileName)
	if err != nil {
		return count, errors.New(
			fmt.Sprintf("Cannot open file <%s>.", g.InputFileName),
		)
	}
	bfi := bufio.NewReaderSize(fi, 1*1024*1024)
	defer fi.Close()

	// -------------------------------------------------------------------------
	// CREATE MAIN INDEXING FILE
	// -------------------------------------------------------------------------
	fo1, err := os.Create(g.InputFileName + ".ggs1")
	if err != nil {
		return count, errors.New(
			fmt.Sprintf("Cannot create main indexing file <%s.ggs1>.",
				g.InputFileName+".ggs1"),
		)
	}
	bfo1 := bufio.NewWriter(fo1)

	count, err = g.createIndexFile1(bfi, bfo1)
	if err != nil {
		return count, err
	}
	fo1.Close()

	// -------------------------------------------------------------------------
	// CREATE DOUBLE INDEXING FILE (THISFILE WILL BE LOADED INTO MEMORY)
	// -------------------------------------------------------------------------
	fi1, err := os.Open(g.InputFileName + ".ggs1")
	if err != nil {
		return count, errors.New(
			fmt.Sprintf("Cannot read main indexing file <%s>.",
				g.InputFileName+".ggs1"),
		)
	}
	bfi1 := bufio.NewReaderSize(fi1, 1*1024*1024)
	defer fi1.Close()

	fo2, err := os.Create(g.InputFileName + ".ggs2")
	if err != nil {
		return count, errors.New(
			fmt.Sprintf("Cannot create double indexing file <%s>.",
				g.InputFileName+".ggs2"),
		)
	}
	bfo2 := bufio.NewWriter(fo2)
	count, err = g.createIndexFile2(bfi1, bfo2)
	if err != nil {
		return count, err
	}
	fo2.Close()
	return count, nil
}

// -------------------------------------------------------------------------
// READ THE INPUT FILE; CREATE MAIN INDEXING
// -------------------------------------------------------------------------
func (g *YiGigaSearch) createIndexFile1(bfi *bufio.Reader, bfo1 *bufio.Writer) (int, error) {
	recCount := 0
	filePos := 0
	for {
		line, err := bfi.ReadString([]byte("\n")[0])
		if err != nil {
			break
		}
		recCount += 1
		if g.PrintStatus == true {
			if recCount%1000 == 0 {
				fmt.Printf("\rCurrently processing main indexing for the line %d", recCount)
			}
		}
		tmpLineArr := strings.Split(line, g.InputFileDelimiter)
		if len(tmpLineArr) < g.InputFileIndex {
			return recCount, errors.New(
				fmt.Sprintf("Index (%d) out of range with the delimiter (%s) at line %d.",
					g.InputFileIndex, g.InputFileDelimiter, recCount),
			)
		}
		indexKey := tmpLineArr[g.InputFileIndex]
		bfo1.WriteString(indexKey + "," + strconv.Itoa(filePos) + "\n")
		filePos += len(line)
	}
	bfo1.Flush()

	if g.PrintStatus == true {
		fmt.Printf("Total of %d records have been indexed.", recCount)
	}
	return recCount, nil
}

// -------------------------------------------------------------------------
// READ THE INPUT FILE; CREATE DOUBLE INDEXING
// -------------------------------------------------------------------------
// func CreateIndexFile2(bfi *bufio.Reader, bfo2 *bufio.Writer, isPrint bool) error {

func (g *YiGigaSearch) createIndexFile2(bfi *bufio.Reader, bfo2 *bufio.Writer) (int, error) {
	pos := 0
	lastIndex := ""
	count := 0
	var err error
	err = nil
	if g.PrintStatus == true {
		for {
			txt, err := bfi.ReadString([]byte("\n")[0])
			if err != nil {
				break
			}
			count += 1

			if count%10000 == 0 {
				fmt.Print(count, "\r")
			}

			if txt[0:g.IndexLen] != lastIndex {
				lastIndex = txt[0:g.IndexLen]
				bfo2.WriteString(lastIndex + "," + strconv.Itoa(pos) + "\n")
			}
			pos += len(txt)
		}
		fmt.Printf("%d indexes have been ranged\n", count)
	} else {
		for {
			txt, err := bfi.ReadString([]byte("\n")[0])
			if err != nil {
				break
			}
			count += 1
			if txt[0:g.IndexLen] != lastIndex {
				lastIndex = txt[0:g.IndexLen]
				bfo2.WriteString(lastIndex + "," + strconv.Itoa(pos) + "\n")
			}
			pos += len(txt)
		}

	}
	bfo2.Flush()
	return count, err
}
